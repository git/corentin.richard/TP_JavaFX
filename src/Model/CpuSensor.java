package Model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class CpuSensor extends SimpleSensor{
    public CpuSensor(float t,String n){
        super(t,n,new CpuGenerator());
    }
    public CpuSensor(Sensor s){
        super(s);
    }

    @Override
    public void genTemp()throws IOException {
        setTemperature(Float.parseFloat(Files.readString(Path.of("/sys/class/thermal/thermal_zone2/temp"))));
    }
}
