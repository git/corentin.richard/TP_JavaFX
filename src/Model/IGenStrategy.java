package Model;

import java.io.IOException;

public interface IGenStrategy {
    abstract float generate() throws IOException;
}
