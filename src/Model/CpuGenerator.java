package Model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class CpuGenerator implements IGenStrategy{

    @Override
    public float generate() throws IOException{
        return Float.parseFloat(Files.readString(Path.of("/sys/class/thermal/thermal_zone2/temp")));
    }
}
