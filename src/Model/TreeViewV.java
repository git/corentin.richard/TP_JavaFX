package Model;

import com.sun.javafx.print.Units;
import javafx.scene.control.TreeItem;

public class TreeViewV implements IVisitor<TreeItem<Sensor>>{
    @Override
    public TreeItem<Sensor> visit(SimpleSensor s){
        return new TreeItem<>(s);
    }

    @Override
    public TreeItem<Sensor> visit(VirtualSensor s){
        TreeItem<Sensor> item = new TreeItem<>(s);
        for (Sensor sensor: s.getSensors().values()){
         item.getChildren().add(sensor.accept(this));
        }return item;
    }
}
