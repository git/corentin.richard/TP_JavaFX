package Model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;

import java.util.Random;

@Deprecated
public class ImageSensor extends SimpleSensor{

    private final ObjectProperty<Image> image = new SimpleObjectProperty<>();
        public ObjectProperty<Image> getImage(){return image;}
        public void setImage(Image image){this.image.set(image);}

    public ImageSensor(float t, String n) {
        super(t, n, new CpuGenerator());
    }

    public ImageSensor(Sensor s){
            super(s);
    }

    @Override
    public void genTemp(){
            setTemperature(15);
            if(getTemperature()<0){
                setImage(new Image("/Img/snow-1674382959288-6329.jpg"));
            }else if(getTemperature()>0 && getTemperature()<20){
                setImage(new Image("/Img/Warm-Green-Landscape-Mountains-Wallpaper-1920x1200-1387564693.jpg"));
            }else {
                setImage(new Image("/Img/videoblocks-panoramic-view-sand-dunes-and-hills-in-hot-desert-wilderness-desert-landscape_h0eyeniim_thumbnail-1080_01.png"));
            }

    }
}
