package Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public class StubLib {
    public static Sensor genSensor(){
        VirtualSensor s = new VirtualSensor(1,"Sensor n°1");
        s.addSensor(new CpuSensor(56,"Cpu n°1"),5F);
        s.addSensor(new CpuSensor(92,"Cpu n°2"),9F);
        s.addSensor(new RandomSensor(6,"Rand n°1"),25F);
        s.addSensor(new RandomSensor(45,"Rand n°2"),12F);
        s.addSensor(new RandomSensor(78,"Rand n°3"),14F);

        return s;
    }
    public static ObservableList<Sensor> genList() {
        List<Sensor> s = new ArrayList<>();
        VirtualSensor v = new VirtualSensor(84, "Sensor n°2");
        v.addSensor(new SimpleSensor(9, "Simple n°1", new CpuGenerator()), 8F);
        v.addSensor(new SimpleSensor(10, "Simple n°2", new RandomGenerator()), 4F);

        VirtualSensor c = new VirtualSensor(7, "Virtual");
        c.addSensor(new SimpleSensor(8, "Simple n°3", new CpuGenerator()), 8F);
        v.addSensor(c, 7F);
        s.add(v);
        s.add(new SimpleSensor(7, "Simple n°4", new RandomGenerator()));
        s.add(new SimpleSensor(7, "Simple n°5", new CpuGenerator()));

        return FXCollections.observableList(s);
    }
}
