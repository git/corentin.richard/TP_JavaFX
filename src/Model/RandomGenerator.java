package Model;

import java.util.Random;

public class RandomGenerator implements IGenStrategy{
    @Override
    public float generate() {
        Random rand = new Random();
        return rand.nextInt(50);
    }
}
