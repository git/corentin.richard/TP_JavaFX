package Model;


import javafx.beans.property.ListProperty;
import javafx.beans.property.MapProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.control.TreeItem;


public class VirtualSensor extends Sensor {
    public VirtualSensor(float t,String n){
        super(t,n);
    }

    private ObservableMap<Float, Sensor> oSensor = FXCollections.observableHashMap();
    private final MapProperty<Float, Sensor> sensors = new SimpleMapProperty<>(oSensor);
    public ObservableMap<Float, Sensor> getSensors() {return sensors.getValue();}


    public void addSensor(Sensor s, Float w) {
        oSensor.put(w, s);
    }

    @Override
    public void genTemp() {
        float total=0;
        if (getSensors().size()==0){
            setTemperature(0);
            return;
        }
        for (ObservableMap.Entry<Float, Sensor> map : oSensor.entrySet()) {
            total+=map.getValue().getTemperature()*map.getKey();
        }
        if (getSensors().size()==0) return;
        total=total/getSensors().size();
        setTemperature(total);
    }

    @Override
    public TreeItem<Sensor> accept(IVisitor v){
        return (TreeItem<Sensor>) v.visit(this);
    }
}
