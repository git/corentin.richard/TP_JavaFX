package Model;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.IOException;
import java.util.UUID;

public abstract class Sensor {
    private final FloatProperty temperature = new SimpleFloatProperty();
        public float getTemperature() {return temperature.get();}
        public FloatProperty temperatureProperty() {return temperature;}
        public void setTemperature(float temperature) {this.temperature.set(temperature);}
    private final StringProperty name = new SimpleStringProperty();
        public String getName() {return name.get();}
        public StringProperty nameProperty() {return name;}
        public void setName(String name) {this.name.set(name);}
    private final StringProperty id = new SimpleStringProperty(UUID.randomUUID().toString());
        public String getId() {return id.get();}
        public StringProperty idProperty() {return id;}
        public void setId(String id) {this.id.set(id);}

    public Sensor(float t,String n){
        this.temperature.set(t);
        this.name.set(n);
    }

    public Sensor(Sensor s){
            this.temperature.set(s.getTemperature());
            this.name.set(s.getName());
            this.id.set(s.getId());
    }

    public abstract void genTemp() throws IOException;
    public abstract <T> T accept(IVisitor<T> v);
    @Override
    public boolean equals(Object sens){
        return getId().equals(((Sensor)sens).getId());
    }
}
