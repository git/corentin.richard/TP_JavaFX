package Model;

import javafx.scene.control.TreeItem;

import java.io.IOException;

public class SimpleSensor extends Sensor{
    private IGenStrategy gen;

    public SimpleSensor(float t, String n,IGenStrategy genStrat) {
        super(t, n);
        this.gen = genStrat;
    }

    public void setStrategy(IGenStrategy g){
        gen = g;
    }

    public SimpleSensor(Sensor s){
        super(s);
    }

    @Override
    public void genTemp() throws IOException {
        setTemperature(gen.generate());
    }

    @Override
    public TreeItem<Sensor> accept(IVisitor v){
        return (TreeItem<Sensor>) v.visit(this);
    }


}
