package Model;

import java.util.Random;

@Deprecated
public class RandomSensor extends SimpleSensor {
    public RandomSensor(float t,String n){
        super(t,n,new RandomGenerator());
    }
    public RandomSensor(Sensor s){
        super(s);
    }

    @Override
    public void genTemp(){
        Random random = new Random();
        setTemperature((random.nextFloat(50)));
    }
}
