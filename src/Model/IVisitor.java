package Model;

public interface IVisitor<T> {
    T visit(SimpleSensor sens);
    T visit(VirtualSensor sens);
}
