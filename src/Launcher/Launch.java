package Launcher;


import Model.StubLib;
import View.TreeViewController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.stage.Stage;

import Model.Sensor;
import View.ImageSensorController;
import Model.ImageSensor;

import java.io.IOException;

public class Launch extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {

        ObservableList<Sensor> list = StubLib.genList();
        TreeViewController tree = new TreeViewController(list,"/FXML/TreeView.fxml","S1");

        Thread t = new Thread(()->{
            while (true){
                try{
                    Thread.sleep(1000);
                    Platform.runLater(()-> {
                        for (Sensor s : list){
                            try{
                                s.genTemp();
                            }catch (Exception e){
                                throw new RuntimeException(e);
                            }
                        }
                    });

                }catch (InterruptedException e){
                    throw new RuntimeException(e);
                }
            }
        });
        t.start();
    }



    public static void main(String args[]) {
        Application.launch(args);
    }
}