package View;

import Model.Sensor;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import javafx.util.converter.NumberStringConverter;

import java.io.IOException;

public class TextSensorController extends FXMLWindow{

    @FXML
    private Text temp;

    public TextSensorController(Sensor s,String url,String title)throws IOException{
        super(url,title);
        temp.textProperty().bindBidirectional(s.temperatureProperty(),new NumberStringConverter());
    }
}
