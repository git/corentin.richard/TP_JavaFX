package View;

import javafx.fxml.FXMLLoader;
import javafx.scene.SubScene;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;


public class FXMLWindow extends Stage {
    public FXMLWindow(String url,String title)throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(url));
        fxmlLoader.setController(this);
        setScene(new Scene(fxmlLoader.load()));
        this.show();
    }
}
