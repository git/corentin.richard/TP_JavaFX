package View;

import Model.Sensor;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.util.converter.NumberStringConverter;
import java.io.IOException;

import Model.ImageSensor;

public class ImageSensorController extends FXMLWindow{

    @FXML
    private Text temp;

    @FXML
    private ImageView img;

    public ImageSensorController(Sensor s,String url,String title) throws IOException{
        super(url,title);
        temp.textProperty().bindBidirectional(s.temperatureProperty(),new NumberStringConverter());
        if(s.getTemperature()<0){
            img.setImage(new Image("Img/snow-1674382959288-6329.jpg"));
        }
        else if(s.getTemperature()>=0 && s.getTemperature()<20){
            img.setImage(new Image("Img/Warm-Green-Landscape-Mountains-Wallpaper-1920x1200-1387564693.jpg"));

        }else{
            img.setImage(new Image("Img/videoblocks-panoramic-view-sand-dunes-and-hills-in-hot-desert-wilderness-desert-landscape_h0eyeniim_thumbnail-1080_01.png"));
        }
    }
}
